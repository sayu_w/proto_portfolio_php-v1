<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="Description" content="Tourat Kévin, développeur web front-end junior"/>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <title>Portfolio</title>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/> 
    <script src="https://kit.fontawesome.com/d6e24bd536.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="assets/css/material-kit.css">
    <link rel="stylesheet" href="dist/css/lightbox.css">
</head>
<body id="home">